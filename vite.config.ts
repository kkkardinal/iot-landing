import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import svgLoader from 'vite-svg-loader'
import autoprefixer from 'autoprefixer';
import path from 'path';

export default defineConfig({
  plugins: [vue(), svgLoader()],
  resolve: {
    alias: {"@": path.resolve(__dirname, "./src")},
  },
  build: { chunkSizeWarningLimit: 1600, },
  css: {
    postcss: {
      plugins: [
        autoprefixer({})
      ],
    }
  }
})
