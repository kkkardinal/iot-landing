import {computed, reactive} from "vue";

const slideState = reactive({
    activeSlideId: 1
});

export function useSlideStore() {
    const setSlideId = (id: number) => {
        slideState.activeSlideId = id
    };

    const slide = computed(() => slideState.activeSlideId);

    return {
        setSlideId,
        slide
    }
}