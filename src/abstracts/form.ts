export interface Form {
    name: string,
    email: string,
    question: string
}