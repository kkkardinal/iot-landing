import { sendMail } from './email';
import {Form} from "../abstracts/form.ts";

export const sendEmailFromContactUs = async (data: Form) => {
    await sendMail(data);
}
