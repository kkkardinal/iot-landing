import { emailConfig } from './emailConfig'
import { Form } from "../abstracts/form.ts";

const emailHost = emailConfig.EMAIL_HOST
const emailApi = '/api/v1/support';

export async function sendMail(data: Form) {
    return fetch(`${emailHost}${emailApi}`, {
        headers: {
            "Content-Type": "application/json",
        },
        method: 'POST',
        body: JSON.stringify(data),
    })
}

